const wrapParagraph = (paragraphs: string[]): JSX.Element[] => {
  return paragraphs.map((paragraph, index) => <p key={index}>{paragraph}</p>);
};

export default wrapParagraph;
