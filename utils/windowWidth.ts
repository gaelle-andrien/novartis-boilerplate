import { breakpoints } from "../assets/styles/mediaqueries";

export const isMobile = (): boolean => {
  return window.innerWidth < breakpoints[1];
};

export const isTablet = (): boolean => {
  return window.innerWidth < breakpoints[2];
};

export const isDesktop = (): boolean => {
  return window.innerWidth < breakpoints[3];
};
