export const brandColor = "#871B1C";

export const defaultTextColor = "#2C2C2C";
export const defaultIconColor = "#FFFFFF";

export const bodyBackgroundColor = "#FFFFFF";
export const footerBackgroundColor = "#FFFFFF";

export const areaPeopleWorkforceColor = brandColor;
export const areaDataTechnologyColor = "#B85495";
export const areaGovernanceRegulatoryColor = "#705387";
export const areaDesignProcessesColor = "#DC873D";
export const areaPartnershipStakeholdersColor = "#F3C251";
export const areaBusinessModelsColor = "#65A356";
