import "@emotion/react";
import { areaPeopleWorkforceColor, areaDataTechnologyColor } from "./colors";

declare module "@emotion/react" {
  export interface Theme {
    colors: {
      primary: string;
    };
  }
}

const theme = {
  PEOPLEWORKFORCE: {
    colors: {
      primary: areaPeopleWorkforceColor,
    },
  },
  DATATECHNOLOGY: {
    colors: {
      primary: areaDataTechnologyColor,
    },
  },
};

export default theme;
