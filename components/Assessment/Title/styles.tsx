import { Theme } from "@emotion/react";
import styled from "@emotion/styled";

interface IStyledTitle {
  theme: Theme;
}

const StyledTitle = styled.h2<IStyledTitle>(({ theme: { colors } }) => ({
  color: colors.primary,
  transition: "all 0.5s ease",
}));

export default StyledTitle;
