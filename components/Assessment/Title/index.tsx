import { useTheme } from "@emotion/react";

import StyledTitle from "./styles";

interface ITitleProps {
  title: string;
}

const Title = ({ title }: ITitleProps): React.ReactElement => {
  const theme = useTheme();

  return <StyledTitle theme={theme}>{title}</StyledTitle>;
};

export default Title;
