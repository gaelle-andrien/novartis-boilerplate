import styled from "@emotion/styled";
import { Theme } from "@emotion/react";

interface IStyledSection {
  theme: Theme;
}
const StyledSection = styled.section<IStyledSection>(({ theme }) => ({
  backgroundColor: theme?.colors.primary,
  transition: "all 0.5s ease",
}));

export default StyledSection;
