import { ThemeProvider } from "@emotion/react";
import { useTranslation } from "next-i18next";
import theme from "../../assets/styles/theme";

import Title from "./Title";

interface IAssessmentProps {
  area: string;
}

const Assessment = ({ area }: IAssessmentProps): React.ReactElement => {
  const { t } = useTranslation();

  return (
    <ThemeProvider theme={theme[area]}>
      <Title
        title={t("home:assessment.question.title", { name: "Jane Doe" })}
      />
    </ThemeProvider>
  );
};

export default Assessment;
