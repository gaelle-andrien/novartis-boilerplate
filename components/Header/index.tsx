/** @jsxImportSource @emotion/react */
import Link from "next/link";
import Logo from "../Logo";
import LanguageSelector from "../LanguageSelector";

import { headerStyle } from "./styles";

interface IHeaderProps {
  title: string;
}

const Header = ({ title }: IHeaderProps): React.ReactElement => (
  <header css={headerStyle}>
    <Link href="/">
      <a>
        <Logo context="nav" title={title} />
      </a>
    </Link>

    <LanguageSelector />
  </header>
);

export default Header;
