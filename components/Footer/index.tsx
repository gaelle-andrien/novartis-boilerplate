/** @jsxImportSource @emotion/react */
import { footerStyle, footerContentStyle } from "./styles";

const Footer = (): React.ReactElement => (
  <footer css={footerStyle}>
    <div css={footerContentStyle} />
  </footer>
);

export default Footer;
