import styled from "@emotion/styled";

import { mq } from "../../assets/styles/mediaqueries";

const size = {
  nav: [
    { width: 75, height: 30 },
    { width: 75, height: 30 },
    { width: 100, height: 40 },
    { width: 100, height: 40 },
  ],
  intro: [
    { width: 300, height: 82.5 },
    { width: 400, height: 110 },
    { width: 500, height: 137.5 },
    { width: 600, height: 165 },
  ],
};

interface IStyledlogo {
  context: string;
}

const Styledlogo = styled.img<IStyledlogo>(({ context }) => {
  const isContextIntro = context === "intro";

  return {
    display: "block",
    width: size[context][0].width,
    height: size[context][0].height,
    margin: isContextIntro && "20px auto 60px",

    [mq[1]]: {
      width: size[context][1].width,
      height: size[context][1].height,
      margin: isContextIntro && "50px auto",
    },

    [mq[2]]: {
      width: size[context][2].width,
      height: size[context][2].height,
      margin: isContextIntro && "60px auto",
    },

    [mq[3]]: {
      width: size[context][3].width,
      height: size[context][3].height,
      margin: isContextIntro && "70px auto",
    },
  };
});

export default Styledlogo;
