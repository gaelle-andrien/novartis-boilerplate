import Styledlogo from "./styles";

interface ILogoProps {
  title: string;
  context: string;
  hasBasline?: boolean;
}

const Logo = ({
  title,
  context,
  hasBasline = false,
}: ILogoProps): React.ReactElement => {
  let url = "/static/logo.svg";

  if (hasBasline) {
    url = "/static/logo-baseline.svg";
  }

  return <Styledlogo src={url} context={context} alt={title} />;
};

export default Logo;
