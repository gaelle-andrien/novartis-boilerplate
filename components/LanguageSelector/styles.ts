
import styled from "@emotion/styled";
import { mq } from "../../assets/styles/mediaqueries";
import { brandColor, defaultTextColor } from "../../assets/styles/colors";

export const languageSelectorStyle = {
  margin: 0,
  padding: 0,
  display: "flex",
};

export const languageSelectorButtonStyle = {
    margin: 0,
    padding: 0,
    paddingTop: 5,
    marginLeft: 5,
    marginRight: 5,
    fontWeight: 700,
    paddingBottom: 5,
    letterSpacing: 2,
    position: "relative" as const,
    transition: "all .2s ease-in-out",
    textTransform: "uppercase" as const,

    [mq[2]]: {
      fontSize: 11,
      paddingTop: 10,
      paddingBottom: 10,
    },

    [mq[3]]: {
      fontSize: 12,
    },
}


interface IStyledLanguageSelectorLink {
  isDisabled: boolean;
}

export const StyledLanguageSelectorLink= styled.a<IStyledLanguageSelectorLink>(
  ({ isDisabled}) => ({
    cursor: isDisabled ? "default" : "pointer",
    color: isDisabled ? defaultTextColor : brandColor,
  })
);