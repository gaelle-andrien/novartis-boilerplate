/** @jsxImportSource @emotion/react */
import Link from "next/link"
import {useRouter} from "next/router";
import { useTranslation } from "next-i18next";

import { languageSelectorStyle, languageSelectorButtonStyle, StyledLanguageSelectorLink } from "./styles";


const LanguageSelector = (): React.ReactElement => {
  const router = useRouter()
  const { t } = useTranslation();

  const allLanguages = router.locales;

  return (
    <ul css={languageSelectorStyle}>
      {allLanguages.map((lang) => (
        <li key={lang} css={languageSelectorButtonStyle}>
            <Link href={router.route} locale={lang}>
              <StyledLanguageSelectorLink isDisabled={router.locale === lang}>
                {t(`common:languageSelector.${lang}`)}
              </StyledLanguageSelectorLink>
            </Link>
        </li>
      ))}
    </ul>
  );
};

export default LanguageSelector;
