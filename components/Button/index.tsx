/** @jsxImportSource @emotion/react */

import Icon, { IIconProps } from "./Icon";

import { Position } from "../../utils/types";

import buttonStyle from "./styles";

interface IButtonProps extends Partial<IIconProps> {
  title: string;
  link?: string;
  onClick?: () => void;
}

function Button({
  title,
  link,
  icon,
  onClick,
  iconSide,
}: IButtonProps): React.ReactElement {
  let leftSideIcon;
  let rightSideIcon;

  if (icon) {
    const buttonIcon = <Icon icon={icon} iconSide={iconSide} />;

    if (iconSide === Position.Right) {
      rightSideIcon = buttonIcon;
    } else {
      leftSideIcon = buttonIcon;
    }
  }

  const innerButton = () => (
    <>
      {leftSideIcon} {title} {rightSideIcon}
    </>
  );

  if (link) {
    return (
      <a href={link} target="_blank" rel="noreferrer" css={buttonStyle}>
        {innerButton()}
      </a>
    );
  }

  return (
    <button type="button" css={buttonStyle} onClick={onClick}>
      {innerButton()}
    </button>
  );
}

export default Button;
