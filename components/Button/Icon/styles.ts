import styled from "@emotion/styled";

import { Position } from "../../../utils/types";

interface IStyledIcon {
  side?: Position;
}

const StyledIcon = styled.span<IStyledIcon>(({ side = Position.Right }) => ({
  marginLeft: side === Position.Right && 10,
  marginRight: side === Position.Left && 10,
}));

export default StyledIcon;
