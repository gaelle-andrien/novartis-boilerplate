import Icon from "../../Icon";

import { Position } from "../../../utils/types";

import StyledIcon from "./styles";

export interface IIconProps {
  icon: string;
  iconSide?: Position;
}

const ButtonIcon = ({ icon, iconSide }: IIconProps): React.ReactElement => (
  <StyledIcon side={iconSide}>
    <Icon icon={icon} size={20} />
  </StyledIcon>
);

export default ButtonIcon;
