import styled from "@emotion/styled";

import { mq } from "../../assets/styles/mediaqueries";
import { layoutMaxWidth } from "../../assets/styles/global";

interface IStyledContainer {
  hasColumns: boolean;
}

const StyledContainer = styled.div<IStyledContainer>(({ hasColumns }) => ({
  zIndex: 1,
  width: "100%",
  display: "flex",
  paddingLeft: 15,
  margin: "0 auto",
  paddingRight: 15,
  position: "relative",
  flexDirection: "column",
  maxWidth: layoutMaxWidth,
  alignItems: "flex-start",

  [mq[1]]: {
    alignItems: hasColumns && "center",
    flexDirection: hasColumns ? "row" : "column",
  },
}));

export default StyledContainer;
