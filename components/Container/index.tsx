import StyledContainer from "./styles";

interface IContainerProps {
  hasColumns?: boolean;
  children: React.ReactNode;
}

const Container = ({
  children,
  hasColumns = false,
}: IContainerProps): React.ReactElement => (
  <StyledContainer hasColumns={hasColumns}>{children}</StyledContainer>
);

export default Container;
