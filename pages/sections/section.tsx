import { GetStaticProps } from "next";
import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

import Button from "../../components/Button";
import Section from "../../components/Section";
import SectionText from "../../components/SectionText";
import SectionTitle from "../../components/SectionTitle";

import { Position } from "../../utils/types";
import wrapParagraph from "../../utils/wrapParagraph";

const PresentationSection = (): React.ReactElement => {
  const section = "section";
  const { t } = useTranslation();

  return (
    <Section id={section}>
      <SectionTitle title={t(`home:${section}.title`)} />
      <SectionText size={100}>
        {wrapParagraph(t(`home:${section}.text`, { returnObjects: true }))}
      </SectionText>
      <br />
      <h3>Button</h3>
      <Button title="[ Button ]" />
      <Button
        title="[ Button left side icon ]"
        icon="arrow_back"
        iconSide={Position.Left}
      />
      <Button
        title="[ Button right side icon ]"
        icon="arrow_forward"
        iconSide={Position.Right}
      />
    </Section>
  );
};

export const getStaticProps: GetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, ["home"])),
  },
});

export default PresentationSection;
