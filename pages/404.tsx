import { GetStaticProps } from "next";
import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

import Head from "../components/Head";
import Section from "../components/Section";
import SectionText from "../components/SectionText";
import SectionTitle from "../components/SectionTitle";

import { Align } from "../utils/types";

const Custom404 = (): React.ReactElement => {
  const { t } = useTranslation();

  return (
    <>
      <Head
        pageURL={t("home:url")}
        pageTitle={t("home:title")}
        pageDescription={t("home:description")}
      />

      <Section align={Align.Center}>
        <SectionTitle title={t("common:error.404.title")} />

        <SectionText size={40}>
          <p>{t("common:error.404.description")}</p>
        </SectionText>
      </Section>
    </>
  );
};

export const getStaticProps: GetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, ["home", "common"])),
  },
});

export default Custom404;
