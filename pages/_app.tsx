import App from "next/app";
import { appWithTranslation } from "next-i18next";

import "../assets/fonts/googleFonts.css";
import globalStyles from "../assets/styles/global";

function MyApp(appProps): React.ReactElement {
  const { Component, pageProps, isIE } = appProps;
  return (
    <>
      {globalStyles}
      <Component {...pageProps} isIE={isIE} />
    </>
  );
}

MyApp.getInitialProps = async (appContext) => {
  const getUserAgent = () => {
    if (appContext.ctx.req) {
      return appContext.ctx.req.headers["user-agent"]; // if you are on the server and you get a 'req' property from your context
    }
    return navigator.userAgent; // if you are on the client you can access the navigator from the window object
  };

  const isIE = (ua) => {
    if (ua) {
      const msie = ua.indexOf("MSIE "); // IE 10 or older => return version number
      const trident = ua.indexOf("Trident/"); // IE 11 => return version number

      if (msie > 0 || trident > 0) return true;
    }

    return false;
  };

  return {
    ...(await App.getInitialProps(appContext)),
    isIE: isIE(getUserAgent()),
  };
};

export default appWithTranslation(MyApp);
