import { GetStaticProps } from "next";
import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

import Head from "../components/Head";
import Section from "../components/Section";
import SectionText from "../components/SectionText";
import SectionTitle from "../components/SectionTitle";

import { Align } from "../utils/types";

interface IErrorProps {
  isIE?: boolean;
  statusCode?: number;
}

const Error = ({ statusCode, isIE }: IErrorProps): React.ReactElement => {
  const { t } = useTranslation();

  let errorMessage = t("common:error.default.client");

  if (statusCode)
    errorMessage = t("common:error.default.server", { statusCode });

  if (isIE) errorMessage = t("common:error.IESupport.description");

  return (
    <>
      <Head
        pageURL={t("home:url")}
        pageTitle={t("home:title")}
        pageDescription={t("home:description")}
      />

      <Section align={Align.Center}>
        {isIE && <SectionTitle title={t("common:error.IESupport.title")} />}

        <SectionText size={40}>
          <p>{errorMessage}</p>
        </SectionText>
      </Section>
    </>
  );
};

Error.getInitialProps = ({ res, err }) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404;
  return { statusCode };
};

export const getStaticProps: GetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, ["home", "common"])),
  },
});

export default Error;
