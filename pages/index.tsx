import { useState } from "react";
import { GetStaticProps } from "next";
import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

import ErrorPage from "./_error";

import Head from "../components/Head";
import Button from "../components/Button";
import Header from "../components/Header";
import Footer from "../components/Footer";
import Container from "../components/Container";
import ScrollTop from "../components/ScrollTop";
import Assessment from "../components/Assessment";

import Section from "./sections/section";

import { Area } from "../utils/types";

interface IHomeProps {
  isIE: boolean;
}

const Home = ({ isIE }: IHomeProps): React.ReactElement => {
  const { t } = useTranslation();
  const [area, setArea] = useState(Area.PeopleWorkforce);

  const onChangeArea = (newArea: Area) => {
    setArea(newArea);
  };

  if (isIE) {
    return <ErrorPage isIE />;
  }

  return (
    <>
      <Head
        pageURL={t("home:url")}
        pageTitle={t("home:title")}
        pageDescription={t("home:description")}
      />

      <Header title={t("home:title")} />

      <main>
        <Container>
          <h1>{t("home:title")}</h1>
          <Section />

          <Button
            title="Button Area People & Workforce"
            onClick={() => onChangeArea(Area.PeopleWorkforce)}
          />
          <Button
            title="Button Area Data & Technology"
            onClick={() => onChangeArea(Area.DataTechnology)}
          />

          <Assessment area={area} />
        </Container>
      </main>

      <ScrollTop />
      <Footer />
    </>
  );
};

export const getStaticProps: GetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, ["home", "common"])),
  },
});

export default Home;
